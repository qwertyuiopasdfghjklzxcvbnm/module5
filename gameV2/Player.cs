﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gameV2
{
    class Player
    {
        public int x = 1;
        public int y = 1;
        public int xp;
        private ConsoleKeyInfo keyInfo;
        private char key;

        public Player(int _xp)
        {
            xp = _xp;
        }
        public void UpdateXP(Trap[] t)
        {
            foreach (Trap tr in t)
            {
                if (tr.x == x && tr.y == y)
                {
                    xp -= tr.damage;
                    if (xp <= 0) throw new Exception("game over");
                }
            }
        }

        public void Move(int sizeMap)
        {
            keyInfo = Console.ReadKey(true);
            key = keyInfo.KeyChar;

            int startingPositionX = x;
            int startingPositionY = y;

            if (key == 'w')
            {
                y--;
            }
            if (key == 'a')
            {
                x--;
            }
            if (key == 'd')
            {
                x++;
            }
            if (key == 's')
            {
                y++;
            }
            if (!OutOfBounds(x, y, 9))
            {
                x = startingPositionX;
                y = startingPositionY;
            }
        }

        private bool OutOfBounds(int x, int y, int size)
        {
            if (x == 0 && y < size && y > 0) return false;
            else if (x == size && y < size && y > 0) return false;
            else if (y == 0 && x < size && x > 0) return false;
            else if (y == size && x < size && x > 0) return false;

            return true;

        }
        public void Victory(int _x, int _y)
        {
            if (x == _x && y == _y) throw new Exception("victory");
        }

    }
}
