﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gameV2
{
    class Trap
    {
        public int x;
        public int y;
        public int damage;

        Random rnd = new Random();

        public Trap(int size, int maxDamage)
        {
            do
            {
                x = rnd.Next(1, size - 1);
                y = rnd.Next(1, size - 1);
            } while ((x == 1 && y == 1) || (x == size - 1 && y == size - 1));

            damage = rnd.Next(1, maxDamage + 1);
        }

    }
}
