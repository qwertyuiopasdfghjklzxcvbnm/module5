﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gameV2
{
    class Map
    {
        private Trap[] t;
        private Player p;

        private int size;


        public Map(int _sizeMap, int quantityTrap, int maxDamage, int xpPlayer)
        {
            size = _sizeMap;
            p = new Player(xpPlayer);
            t = new Trap[quantityTrap];

            for (int i = 0; i < quantityTrap; i++)
            {
                t[i] = new Trap(_sizeMap, maxDamage);

                for (int j = 0; j < i; j++)//что бы мины не спавнились одна на одной
                {
                    if (t[i].x == t[j].x && t[i].y == t[j].y)
                    {
                        t[i] = null;
                        i--;
                        break;
                    }

                }
            }

        }

        public void UpdateMap()
        {
            Console.Clear();
            DrawMap();

            //foreach (Trap tr in t) // показать мины
            //{
            //    Console.SetCursorPosition(tr.x, tr.y);
            //    Console.Write("0");
            //}
            Console.SetCursorPosition(size - 2, size - 2);
            Console.Write("+");

            p.UpdateXP(t);
            Console.SetCursorPosition(20, 0);
            Console.Write(p.xp + " xp");

            Console.SetCursorPosition(p.x, p.y);
            Console.Write("o");
            p.Victory(size - 2, size - 2);
            p.Move(size - 1);
        }

        private void DrawMap()
        {
            int i = 0, j = 0;

            int k = 0;

            for (k = 0; k < size - 1; k++)
            {
                Console.SetCursorPosition(i, j++);
                Console.Write("#");
            }

            for (k = 0; k < size - 1; k++)
            {
                Console.SetCursorPosition(i++, j);
                Console.Write("#");
            }

            for (k = 0; k < size - 1; k++)
            {
                Console.SetCursorPosition(i, j--);
                Console.Write("#");
            }

            for (k = 0; k < size - 1; k++)
            {
                Console.SetCursorPosition(i--, j);
                Console.Write("#");
            }
        }

    }
}
